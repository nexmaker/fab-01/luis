# gitlab 初次使用记录
时间：2020-03-21
环境如下：
笔记本：HUAWEI MateBook E 2019款
处理器：Snapdragon (TM) 850 
操作系统：WIN10 ARM64


注意：因为操作系统是win 10 arm64，目前还不兼容主流64位软件，大多数就只能装32位的（这也导致后续安装一款picgo图床工具时发生了大麻烦，找不到32位的安装包，无法使用picgo）

# 先进行准备工作，账号注册，软件安装。
## gitlab注册
https://www.gitlab.com/
进入gitlab，注册账号并登陆。



## 安装git，并使用（文件的）
https://git-scm.com/
在官网下载并安装后，Git Bash。可以在桌面空白处，点击鼠标右键，然后选择Git Bash Here。或者到开始菜单，找到刚安装的git目录，打开Git Bash。如下图所示：
![](https://gitlab.com/picbed/bed/uploads/34832d7b441c9440018ded951b030feb/gitbash.jpg) ![](https://gitlab.com/picbed/bed/uploads/2c4a64d41ef73372bcca9930171f7a6f/git_bash2.jpg)

* 1、在本地电脑上新建一个名称为book的文件夹，我的目录为C:/User/luis/Documents/book。(可以自定义目录位置和文件名，但是要记住，此后这个目录作为存放git文件的目录)
* 2、在git bash中，移动到目标目录（/C:/User/luis/Documents/book为目标目录）。命令如下：
```
cd /C:/User/luis/Documents/book
```
* 3、初始化本地git库。 Initialized empty Git repository 命令如下：
```
git init
```
* 4、打开Git Bash后，链接到云端的gitlab。（下面的XXX和XXX@xx.com是我在gitlab的用户名和邮箱）命令如下：
``` 
git config user.name "XXX"  
git config user.email "XXX@xx.com":
```
* 5、将gitlab云端的文件全部clone到本地。其中git@gitlab.com:nex-fab/fab-01/luis.git是我的个人SSH地址，可以在gitlab的projects中找到并复制。命令与获取SSH地址如下：
```
git clone git@gitlab.com:nex-fab/fab-01/luis.git
```
![](https://gitlab.com/picbed/bed/uploads/9696ac541a824d24886498464f5d46cd/clone1.jpg)
![](https://gitlab.com/picbed/bed/uploads/9f91827f8626f1c7544e6c674f5698b6/clone2.jpg)

因为是首次使用，SSH key没有配置好，因此提示找不到 'fatal: Could not read from remote repository.'
接下来配置SSH key，先移动至.ssh目录
```
cd ~/.ssh
```
创建一对新的SSH密钥(keys)。命令如下：
```
ssh-keygen -t rsa -C "your_email@example.com"
```
Generating public/private rsa key pair.
Enter file in which to save the key (/c/Users/luis/Documents/book/.ssh/id_rsa): [Press enter]
直接回车，则将密钥按默认文件进行存储。此时也可以输入特定的文件名，比如/c/Users/you/.ssh/github_rsa
接着，根据提示，你需要输入密码和确认密码。相关提示如下：
Enter passphrase (empty for no passphrase): [Type a passphrase]
Enter same passphrase again: [Type passphrase again]
输入完成之后，屏幕会显示如下信息：
Your identification has been saved in /c/Users/luis/Documents/book/.ssh/id_rsa.
Your public key has been saved in /c/Users/luis/Documents/book/.ssh/id_rsa.pub.
The key fingerprint is:
01:0f:f4:3b:ca:85:d6:17:a1:7d:f0:68:9d:f0:a2:db your_email@example.com

输入命令来查看钥匙，看到id_rsa、id_rsa.pub、known_hosts三个文件。
```
ls ~/.ssh/
```
![](https://gitlab.com/picbed/bed/uploads/667bcf9857b1f157beab43aa8dca8ef7/ssh_key1.jpg)
我们只需要使用公共钥匙就行了。输入命令查看并复制公共钥匙的全部内容。：
```
cat ~/.ssh/id_rsa.pub
```
![](https://gitlab.com/picbed/bed/uploads/a5927c7f706f1fd8033c235fbf87ee44/ssh_key2.jpg)
在gitlab中填写SSH key并保存，如图所示：
![](https://gitlab.com/picbed/bed/uploads/7ddb8f726de9a7ca4b179fa0d5fee440/ssh_key3.jpg)
钥匙保存之后，再次输入git clone命令，没有报错，看到done就说明下载成功了。
![](https://gitlab.com/picbed/bed/uploads/916d383868fc978680729b4f3689674a/git_clone.png)
* 6、将云端文件更新到本地
```
git pull origin master
```
* 7、打开C:/User/luis/Documents/book/luis/book文件夹新建一个test01文件夹，在test01文件夹下新建一个practice.md文件，并用记事本打开随便输入一些内容保存。
* 8、使用命令将所有文件更新到云端。
选择添加文件。
```
git add --all
```
![](https://gitlab.com/picbed/bed/uploads/8271cb2e5851f4bb16a46cd55c742d47/git_push1.png)
对本次更新进行备注，XXX为自己的备注内容。
```
git commit -m "XXX"
```
![](https://gitlab.com/picbed/bed/uploads/377225b9b524081cc30f3af18c5cf2d8/git_push2.png)
使用push命令。
```
git push origin master
```
![](https://gitlab.com/picbed/bed/uploads/1dcabe07f0fb64aa83118ebd2f0046a7/git_push3.png)
看到这个报错，是因为目录位置不对。因为前面clone下载文件后，将我的所有文件都在book目录中新生成了一个名为luis的子目录，因此需要移动到这个luis目录，跟云端内容一一对应才行。下面就移动到正确目录,然后再push。
```
cd /C:/User/luis/Documents/book/luis
```
输入git push命令，然后输入密码（密码不显示，只要输入即可），输入完成按Enter键，然后就会上传至云端。没有报错，看到done就说明成功了。
```
git push origin master
```
![](https://gitlab.com/picbed/bed/uploads/5cde8e8134d13c09cfddd7897d65e924/git_push4.png)
看到所有东西都已经更新，说明成功了，此时可以在gitlab网站上，看到已经更新的内容。

## 安装vscode（用markdown插件来进行网页的编辑）
https://code.visualstudio.com/
打开官网，下载并安装后，找到并打开安装目录中的code.exe文件。
安装markdown插件。过程如下图所示：
![](https://gitlab.com/picbed/bed/uploads/e64e72bc4ca8f787304dae9502202c04/markdown1.jpg)
![](https://gitlab.com/picbed/bed/uploads/2f414196490ec897eb0ac18f6e54ed9a/markdown2.jpg)
![](https://gitlab.com/picbed/bed/uploads/34fed26a53891fe8b37a802a9bf166d6/markdown3.jpg)
下图的第3步是点击红框中的install按钮，因为我已经安装完成了，所以显示的是齿轮按钮。
![](https://gitlab.com/picbed/bed/uploads/177167cba70591f9f3eab0c608edbd03/markdown4.jpg)

在VScode中打开在C:/User/luis/Documents/book/luis/book文件夹，并在book目录下新建一个test01文件夹，在test01文件夹下新建一个practice.md文件。

![](https://gitlab.com/picbed/bed/uploads/4e59314456bba9e51cda038acbffb6f0/vscode-open_folder.jpg)
![](https://gitlab.com/picbed/bed/uploads/6b4e0da1cf085b471d36f74bc41c47ff/vscode-new_folder.jpg) ![](https://gitlab.com/picbed/bed/uploads/ca4368872571a20e990fa680301b9d5f/vscode-new_file.jpg)
网站编辑好之后，可以推送程序。

## 安装PicGo（用作图床工具）
下载网址 https://github.com/Molunerfinn/PicGo/releases 
windows下载直链 https://github.com/Molunerfinn/PicGo/releases/download/v2.2.2/PicGo-Setup-2.2.2.exe
由于本电脑后来无法安装picgo，最终选择在另外一台电脑上安装了picgo，用于图片的上传。
如果PicGO配置出现问题，可以参考这里→[PicGo配置手册](https://picgo.github.io/PicGo-Doc/zh/guide/config.html#%E5%9F%BA%E6%9C%AC%E6%93%8D%E4%BD%9C%E9%A2%84%E8%A7%88)

安装完成后，在默认的图床里面没有找到gitlab.
于是在插件设置中，搜索gitlab，并选择安装。
但是一直显示安装中，等了许久都，没有安装成功。
上网查找相关资料，发现必须安装[Node.js](https://nodejs.org/en/)之后才能安装PicGo的插件，因为PicGo要使用npm来安装插件。
成功安装Node.js之后，重新打开picgo，然后再去安装gitlab插件，就成功安装上了。
接下来打开gitlab，按照下图进行操作。
![](https://gitlab.com/picbed/bed/uploads/8d019108ffa27187c60092d032c63601/picgo.jpg)
操作完成之后，你会看到一个token，复制它。
![](https://gitlab.com/picbed/bed/uploads/d26961c545a0b01ccf0fcbfbb3cd21a1/token.jpg)

打开picgo，进行gitlab图床设置。
依此填写URL(https://gitlab.com)、Group(Group name)、Project(Project name)、Token(Token just copied).
![](https://gitlab.com/picbed/bed/uploads/0e596148c178b2f683136fb4cd9ecd18/picgo-gitlab.jpg)

填写完成后点击确定，就可以打开上传区去上传图片了。
点击上传图片，选择图片即可上传。上传成功后，可以在相册一栏看到已经上传的图片。
![](https://gitlab.com/picbed/bed/uploads/147e0cef3c464585a4205d906c1e45df/picgo-photo.png)

这样就可以使用gitlab作为图床工具了。