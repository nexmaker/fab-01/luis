# gitlab record of first use
time：2020-03-21  
environment：  
laptop：HUAWEI MateBook E 2019  
CPU：Snapdragon (TM) 850   
Operating System：WIN10 ARM64  

Note: because the operating system is Windows 10 arm64, it is not compatible with mainstream 64-bit software, so most of them can only be installed with 32 bits (this also led to a big trouble in the subsequent installation of a picgo tool, the 32-bit installation package could not be found, so picgo cannot be used).

# Preparation work (account registration, software installation)

## gitlab registered
https://www.gitlab.com/  
Open the gitlab website to register and log in.


## Install git and use it (some basic instructions)
https://git-scm.com/  
After downloading and installing from the official website, you can right-click in the desktop space and select Git Bash Here. Or go to the start menu, locate the git directory you just installed, and open git Bash. As shown in the following figure:  
![](https://gitlab.com/picbed/bed/uploads/34832d7b441c9440018ded951b030feb/gitbash.jpg) ![](https://gitlab.com/picbed/bed/uploads/2c4a64d41ef73372bcca9930171f7a6f/git_bash2.jpg)

* 1、Create a new folder named book on your local computer, my directory to C:/User/luis/Documents/book.  
  (You can customize the directory location and file name, but keep in mind that this directory will be the directory for git files from now on.)  
* 2、In git bash, move to the target directory. The command is as follows:  
  (Will be C: / User/luis/Documents/book as the target directory.)  
```
cd /C:/User/luis/Documents/book
```
* 3、 Initialized empty Git repository. The command is as follows:  
```
git init
```
* 4、After opening Git Bash, link to gitlab in the cloud.  
  (the following XXX and XXX@xx.com are my user name and email address in gitlab)  
``` 
git config user.name "XXX"  
git config user.email "XXX@xx.com":
```
* 5、Clone all gitlab cloud files locally.  
  Where git@gitlab.com:nex-fab/fab-01/luis.git is my personal SSH address, which can be found and copied in gitlab's projects.  
Command and SSH are obtained as follows:  
```
git clone git@gitlab.com:nex-fab/fab-01/luis.git
```
![](https://gitlab.com/picbed/bed/uploads/9696ac541a824d24886498464f5d46cd/clone1.jpg)
![](https://gitlab.com/picbed/bed/uploads/9f91827f8626f1c7544e6c674f5698b6/clone2.jpg)

Because this is the first time, the SSH key is not configured properly, so the prompt cannot be found 'fatal: Could not read from remote repository.'  
Next, configure SSH key and move to the. SSH directory.  
```
cd ~/.ssh
```
Create a new pair of SSH keys.
```
ssh-keygen -t rsa -C "your_email@example.com"
```
Generating public/private rsa key pair.  
Enter file in which to save the key (/c/Users/luis/Documents/book/.ssh/id_rsa): [Press enter]  


<font face="黑体" color=grey>Direct return stores the key as the default file. At this point, you can also enter a specific file name, such as /c/Users//luis/Documents/book/.ssh/github_rsa
Next, you are prompted to enter your password and confirm it.Relevant tips are as follows:</font>  


Enter passphrase (empty for no passphrase): [Type a passphrase]  
Enter same passphrase again: [Type passphrase again]  


<font face="黑体" color=grey>When the input is complete, the screen will display the following information:</font>  


Your identification has been saved in /c/Users/luis/Documents/book/.ssh/id_rsa.  
Your public key has been saved in /c/Users/luis/Documents/book/.ssh/id_rsa.pub.  
The key fingerprint is:  
01:0f:f4:3b:ca:85:d6:17:a1:7d:f0:68:9d:f0:a2:db your_email@example.com  


Enter the command to view the key, and you will see three files: id_rsa, id_rsa.pub, and known_hosts.
```
ls ~/.ssh/
```
![](https://gitlab.com/picbed/bed/uploads/667bcf9857b1f157beab43aa8dca8ef7/ssh_key1.jpg)  
All we need is a public key. Enter the command to view and copy the full contents of the public key.:  
```
cat ~/.ssh/id_rsa.pub
```
![](https://gitlab.com/picbed/bed/uploads/a5927c7f706f1fd8033c235fbf87ee44/ssh_key2.jpg)  
Fill in the SSH key in gitlab and save it, as shown in the figure:
![](https://gitlab.com/picbed/bed/uploads/7ddb8f726de9a7ca4b179fa0d5fee440/ssh_key3.jpg)  
Once the key is saved, type the git clone command again. No error was reported, and when you see done, it means the download was successful.
![](https://gitlab.com/picbed/bed/uploads/916d383868fc978680729b4f3689674a/git_clone.png)  
* 6、Copy cloud files to update locally  
```
git pull origin master
```
* 7、In the C:/User/luis/Documents/book/luis/book folder to create a new test01 folder, create a practice.md under test01 folder file, and use notepad to open it, just enter some content and save.  
* 8、Update all files to the cloud.  
Select the file you want to add  
```
git add --all
```
![](https://gitlab.com/picbed/bed/uploads/8271cb2e5851f4bb16a46cd55c742d47/git_push1.png)  
Comment on this update, XXX is its own remarks.  
```
git commit -m "XXX"
```
![](https://gitlab.com/picbed/bed/uploads/377225b9b524081cc30f3af18c5cf2d8/git_push2.png)  
Use the push command  
```
git push origin master
```
![](https://gitlab.com/picbed/bed/uploads/1dcabe07f0fb64aa83118ebd2f0046a7/git_push3.png)  
See this error because the directory location is wrong.Since clone created a new subdirectory named luis in the book directory after downloading the file, I need to move to the luis directory, which corresponds to the contents in the cloud.Let's move to the correct directory and then push.  
```
cd /C:/User/luis/Documents/book/luis
```
Enter the git push command, then Enter the password (the password is not displayed, as long as the input can be), input completed press Enter, and then will be uploaded to the cloud.No error was reported. Seeing done means success.  
```
git push origin master
```
![](https://gitlab.com/picbed/bed/uploads/5cde8e8134d13c09cfddd7897d65e924/git_push4.png)  
The upload is successful and you can now see the updated content on the gitlab website.  

## Install vscode (markdown plugin for editing web pages)
https://code.visualstudio.com/  
Open the official website, download and install it, find the code.exe file in the installation directory and open it.  
Install the markdown plug-in.The process is shown in the following figure:  
![](https://gitlab.com/picbed/bed/uploads/e64e72bc4ca8f787304dae9502202c04/markdown1.jpg)  
![](https://gitlab.com/picbed/bed/uploads/2f414196490ec897eb0ac18f6e54ed9a/markdown2.jpg)  
![](https://gitlab.com/picbed/bed/uploads/34fed26a53891fe8b37a802a9bf166d6/markdown3.jpg)  
Step 3 below is to click the install button in the red box, which shows the gear button because I have already installed it.  
![](https://gitlab.com/picbed/bed/uploads/177167cba70591f9f3eab0c608edbd03/markdown4.jpg)  
In VScode open in C:/User/luis/Documents/book/luis/book folder, and then in the book to create a new directory test01 folder, under test01 folder to create a new practice.md file.  

![](https://gitlab.com/picbed/bed/uploads/4e59314456bba9e51cda038acbffb6f0/vscode-open_folder.jpg)  
![](https://gitlab.com/picbed/bed/uploads/6b4e0da1cf085b471d36f74bc41c47ff/vscode-new_folder.jpg) ![](https://gitlab.com/picbed/bed/uploads/ca4368872571a20e990fa680301b9d5f/vscode-new_file.jpg)  
Once the file is edited, save it and update git bash to the cloud.  

## Install PicGo (as a map bed tool)
Download url: https://github.com/Molunerfinn/PicGo/releases  
Windows downloads straight chain: https://github.com/Molunerfinn/PicGo/releases/download/v2.2.2/PicGo-Setup-2.2.2.exe  
Since picgo could not be installed on this computer, I finally installed picgo on another computer for uploading pictures.  
If there is a problem with the PicGO configuration, see here→[PicGo configuration manual](https://picgo.github.io/PicGo-Doc/zh/guide/config.html#%E5%9F%BA%E6%9C%AC%E6%93%8D%E4%BD%9C%E9%A2%84%E8%A7%88)

After installation, gitlab was not found in the default map bed.  
So in the plug-in Settings, search gitlab and select install.  
But has been showing the installation, waiting for a long time, did not install successfully.  

Search the Internet, found that you must install [Node.js](https://nodejs.org/en/) after the installation of PicGo plug-in, because PicGo to use NPM to install the plug-in.  
After a successful installation of node. js, reopen picgo, and then go install the gitlab plug-in, which is quickly installed.  
Next, open gitlab and follow the following figure.  
![](https://gitlab.com/picbed/bed/uploads/8d019108ffa27187c60092d032c63601/picgo.jpg)  
When the operation is complete, you will see a token and copy it.  
![](https://gitlab.com/picbed/bed/uploads/d26961c545a0b01ccf0fcbfbb3cd21a1/token.jpg)  

Open picgo and set the gitlab graph bed.  
Fill in URL(https://gitlab.com) 、Group(Group name)、Project(Project name)、Token(Token just copied).  
![](https://gitlab.com/picbed/bed/uploads/0e596148c178b2f683136fb4cd9ecd18/picgo-gitlab.jpg)  

Click ok after filling in, you can open the upload area to upload pictures.  
Click upload picture, select the picture to upload.After the upload is successful, you can see the uploaded pictures in the album column.    
![](https://gitlab.com/picbed/bed/uploads/147e0cef3c464585a4205d906c1e45df/picgo-photo.png)  
This makes it possible to use gitlab as a map bed tool.  