# arduino练习
本次温度检测的练习由我与herman共同参与完成。
 
## 实物连接 
本次练习使用的材料分别是[DM-OLED096-624](https://www.displaymodule.com/collections/oled/products/dm-oled096-624)显示模块、arduino Uno板子、TMP36gz温度传感器、面包板、杜邦线。  
TMP36gz温度传感器实物图  
![](https://gitlab.com/picbed/bed/uploads/ce5c0921c69ff6c3ceaadbb7ebc4a15a/温度传感器0.jpg)  
DM-OLED096-624显示模块实物图  
![](https://gitlab.com/picbed/bed/uploads/48f0fba469208e10bab6e20ea4219eec/oled-096-624实物图.jpg)  
arduino Uno板子实物图  
![](https://gitlab.com/picbed/bed/uploads/be00c97e63d8a21fcd58dcdf321dc576/板子实物.jpg)  
[DM-OLED096-624连线参考](https://www.displaymodule.com/collections/oled/products/dm-oled096-624)  
![](https://gitlab.com/picbed/bed/uploads/89f1f876e8fe052047195f8ef3c731d1/OLED096-624连线.png)  
[温度传感器连线参考](https://www.nexmaker.com/doc/5arduino/arduino_code.html)  
![](https://gitlab.com/picbed/bed/uploads/e57d9b1cf47d6b616e1eb893658c7a78/TMP36GZ连线图.jpg)  
实物连线图  
![](https://gitlab.com/picbed/bed/uploads/66a1f9c95f52adbf110d3cbb837874f2/实物连线图.jpg)  

## 安装arduino
[arduino官网](https://www.arduino.cc/en/Main/Software)   
我的电脑之前就安装过arduino，因此不再重复安装。  

## 导入库文件
DM-OLED096-624需要搭配U8g2库使用。[库文件下载地址](https://github.com/olikraus/U8g2_Arduino)  
![](https://gitlab.com/picbed/bed/uploads/ce6e0d6df4c969d4ee1837e06a83225b/下载库文件.png)  
下载后，将zip压缩包中的内容解压到arduino的库文件目录。  
默认目录是在“C:\Users\ <font color=red>XXX</font>\Documents\Arduino\libraries”  //这里的<font color=red>XXX</font>是本地电脑的用户名。  
也可以放到安装目录中，我这里arduino软件安装在D盘，目录是D:\Arduino\libraries  


## 修改代码  
本次练习参考代码是U8g2库中的案例U8g2_Arduino-master\examples\page_buffer\GraphicsTestGraphicsTest.ino  
此外还参考了[Nexmaker的温度传感器案例](https://www.nexmaker.com/doc/5arduino/arduino_code.html)  
```  
const int temperaturePin = 0;

void setup()
{ 
  Serial.begin(9600);
}

void loop()
{

  float voltage, degreesC, degreesF;

  voltage = getVoltage(temperaturePin);
  degreesC = (voltage - 0.5) * 100.0;
  degreesF = degreesC * (9.0/5.0) + 32.0;

  Serial.print("voltage: ");
  Serial.print(voltage);
  Serial.print("  deg C: ");
  Serial.print(degreesC);
  Serial.print("  deg F: ");
  Serial.println(degreesF);

  delay(1000); // repeat once per second (change as you wish!)
}

float getVoltage(int pin)
{

  return (analogRead(pin) * 0.004882814);

}
```  
最终的代码如下：  
```  
#include <Arduino.h>
#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
const int temperaturePin = 0;
U8G2_SSD1306_128X64_NONAME_1_4W_SW_SPI u8g2(U8G2_R0, /* clock=*/ 13, /* data=*/ 11, /* cs=*/ 10, /* dc=*/ 9, /* reset=*/ 8);
void u8g2_prepare(void) {
  u8g2.setFont(u8g2_font_6x10_tf);
  u8g2.setFontRefHeightExtendedText();
  u8g2.setDrawColor(1);
  u8g2.setFontPosTop();
  u8g2.setFontDirection(0);
}

void u8g2_extra_page(uint8_t a)
{
  u8g2.drawStr( 0, 0, "TEMP");
  u8g2.setFont(u8g2_font_unifont_t_symbols);
  u8g2.setFontPosTop();
  u8g2.drawUTF8(0, 24, "☀ ☁");
  u8g2.drawUTF8(48,0,"V: ");
  u8g2.drawUTF8(48,24,"C: ");
  u8g2.drawUTF8(48,48,"F: ");
  switch(a) {
    case 0:
    case 1:
    case 2:
    case 3:
      u8g2.drawUTF8(a*3, 36, "☂");
      break;
    case 4:
    case 5:
    case 6:
    case 7:
      u8g2.drawUTF8(a*3, 36, "☔");
      break;
  }
}

uint8_t draw_state = 0;
void draw(void) {
  u8g2_prepare();
  switch(draw_state >> 3) {
    case 0: u8g2_extra_page(draw_state&7); break;
    case 1: u8g2_extra_page(draw_state&7); break;
    case 2: u8g2_extra_page(draw_state&7); break;
    case 3: u8g2_extra_page(draw_state&7); break;
    }
}

void setup(void) {
  /* U8g2 Project: SSD1306 Test Board */
  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);
  digitalWrite(10, 0);
  digitalWrite(9, 0);		

  u8g2.begin();
  //u8g2.setFlipMode(0);
  Serial.begin(9600);
}

void loop(void) {
  // picture loop  
  float voltage, degreesC, degreesF;
  voltage = getVoltage(temperaturePin);
  degreesC = (voltage - 0.5) * 100.0;
  degreesF = degreesC * (9.0/5.0) + 32.0;
  u8g2.firstPage();  
  do {
    draw();
    u8g2.setCursor(65,0);
    u8g2.print(voltage);
    u8g2.setCursor(65,24);
    u8g2.print(degreesC);
    u8g2.setCursor(65,48);
    u8g2.println(degreesF);
  } while( u8g2.nextPage() );
  delay(200); 
  // increase the state
  draw_state++;
  if ( draw_state >= 5*5 )
    draw_state = 0;
}

float getVoltage(int pin)
{
  return (analogRead(pin) * 0.004882814);
}
```  

## 显示效果图
![](https://gitlab.com/picbed/bed/uploads/cb7b13e28f471149dc46555af00c7ccd/效果.gif)  